// Section Comparison Query Operators

// $gt/$gte operator (greater than/greater than or equal to)
/*
- allows us to find documents that have field number values greater than or greater than or equal to a specified number
Syntax:
	db.collectionName.find({field: {$gt:value}});
	db.collectionName.find({field: {$gte:value}});
*/

db.users.find({age:{$gt: 50}});
db.users.find({age:{$gte: 50}});


// $lt/$lte operator (less than/less than or equal to)
/*
- allows us to find documents that have field number values less than or less than or equal to a specified number
Syntax:
	db.collectionName.find({field: {$lt:value}});
	db.collectionName.find({field: {$lte:value}});
*/
db.users.find({age:{$lt: 50}});
db.users.find({age:{$lte: 50}});

// $ne operator (not equal)
// allows us to find docs that have field number values that are not equal to a specified number
/*
Syntax
	db.collectionName.find({field:{$ne:value}});
*/
db.users.find({age:{$ne: 82}});

// $in operator
/*
-allows us to find documents with specific math criteria on one field using different values
Syntax:
	db.collectionName.find(field: {$in: value});
*/
db.users.find({lastname: {$in: ["Hawking", "Smith"]}});


// when looking for a nested array using $in operator
db.users.find({courses: {$in: ["HTML", "React"]}});

// Section - Logical Query Operators
// $or operator
/*
- allows us to find documents matching a single criteria from multiple provided search criteria
Syntax:
	db.collectionName.find({$or:[{filedA: valueA}, {fieldB: valueB}]});
*/
db.users.find({$or: [{firstName: "Neil"}, {age: 21}]});

// kung combination ng operators:
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 21}}]});

// $and operator
/*
- allows us to find documents matching multiple criteria in a single field
Syntax:
	db.collectionName.find({$and: [{fieldA:valueA}, {fieldB:valueB}]});
*/

db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});

// Section - Field Projections
/*
-retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response
- when dealing with complex data structures, there might be instances when fields are not useful for the query that we are trying to accomplish.
-to help with the readability of the values returned, we can include/exclude fields from the response
*/

// Inclusion
/*
- allows us to include specific fields only when retrieving documents
- the value provided is 1 to denote that the field is being included. 1 means true, so pwede din true ang gamitin instead na 1
Syntax:
	db.collectionName.find({criteria}, {field: 1});

*/
db.users.find(
	{firstName: "Jane"},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

// yung response na irereturn lang ang binabago, pero yung data same pa rin 

// Exclusion
/*
-allows us to exclude specific fields only when retrieving documents
- the value provided is 0 to denote that the field is being excluded, pwede din false
Syntax:
	db.collectionName.find({criteria}, {field:0});

*/

db.users.find(
	{firstName: "Jane"},
	{
		contact: 0,
		department: 0
	}
);

// Suppressing of ID field
// kasi madalas hindi natin dapat idisclose yung ID ni user lalo kung hexadecimal na ID
/*
- when using field projections, field inclusion and exclusion may be used at the same time
- this is only applicable if we are suppressing the "_id" field.
- kay id lang ito mag-work
*/

db.users.find(
	{firstName: "Jane"},
	{
		_id: 0,
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

// try natin kay age. nagka-error

// Returning Specific Fields in EMbedded Documents

// one way of coding it
db.users.find(
	{firstName: "Jane"},
	{
		firstName: true,
		lastName: true,
		contact: {
			phone: true
		}
	}
);

// another way of coding it
db.users.find(
	{firstName: "Jane"},
	{
		firstName: 1,
		lastName: 1,	
		"contact.phone": 1
	}
);

// to suppress specific fields in embedded documents
db.users.find(
	{firstName: "Jane"},
	{
		"contact.phone": 0
	}
);

// either gagamit ka nng JSON.notation or objects

// Project Specific Array Elements in the Returned Array

// $slice operator - allows us to retrieve only 1 element that matches the search criteria

db.users.find(
	{
		"nameArr":{nameA: "Juan"},
	},
	{
		nameArr:
			{$slice : 1}
	}
);

// Section - Evaluation Query Operators
// $regex operator
/*
- allows us to find documents that match a specific string pattern using regular expressions
Syntax:
	db.collectionName({field: $regex: "pattern", $options: "$optionsValue"});
*/

db.users.find({firstName: {$regex: "N"}});
// case-sensitive kasi si MongoDB kaya gagamitin mo itong sa baba para mabasa niya at maging case-insensitive. yung options mean may special instructions na may gusto ka pang sabihin re: pinapahanap sa $regex
db.users.find({firstName: {$regex: "j", $options: "$i"}});

// using or
db.users.find({$or:[{firstName: { $regex: "n"}},{firstName: { $regex: "N"}}]});
